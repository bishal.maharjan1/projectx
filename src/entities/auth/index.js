import React from 'react';
import {useForm} from "react-hook-form";
import styles from './style.module.scss';
import {
    useHistory,
} from "react-router-dom";

import {gql, useMutation} from '@apollo/client';

const ADD_USER = gql`
    mutation ADD_USER($username: String!, $role: String!) {
      insert_user(objects:{username: $username, role: $role}) {
        returning {
          username
          role
          id
        }
      }
    }
`;

const Auth = (props) => {
    const {register, handleSubmit, errors} = useForm();
    const [addUser, {data}] = useMutation(ADD_USER);

    const history = useHistory();

    const onSubmit = async (formdata) => {
        let res = await addUser({variables: {username: formdata.username, role: formdata.role}});
        if (res) {
            window.localStorage.setItem('username', res.data.insert_user.returning[0].username);
            window.localStorage.setItem('role', res.data.insert_user.returning[0].role);
            history.push('/home')
        }
    };

    return (
        <div className={styles["main-container"]}>
            <div className={styles["signup-container-cover"]}>
                <h1>Retrospective</h1>
                <img src="5495.jpg" alt="login_illustration"/>
                <h3>"One Vote, One Contribution."</h3>
            </div>
            <div className={styles["signup-container-form-container"]}>
                <h1>Login </h1>
                <form onSubmit={handleSubmit(onSubmit)}
                      className={styles["form-container"]}>
                    Username <input name="username"
                                    ref={register({required: true})}
                                    style={{margin: '10px 0px'}}
                                    placeholder={'username'}
                />
                    {errors.username && 'Username is required.'}

                    <select name="role" ref={register}>
                        <option value="guest">Guest</option>
                        <option value="admin">Admin</option>
                    </select>

                    <input type="submit" className={styles.button}/>
                </form>
            </div>

        </div>
    )
};

export default Auth;
