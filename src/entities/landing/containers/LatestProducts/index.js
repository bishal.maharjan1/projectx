import React, {useEffect} from "react";
import {connect} from "react-redux";
import ProductCard from "../../components/ProductCard/ProductCard";

const LatestProducts = props => {
    useEffect(() => {
        console.log('loadData');
    }, [])
    return (
        <div className={'products-list'}>
            {props.el.map((val,key)=>(
                <ProductCard product={val} />
            ))}
        </div>
    );
};

const mapStateToProps = state => {
};
const mapDispatchToProps = dispatch => {
};
export default connect(mapStateToProps, mapDispatchToProps)(LatestProducts);