import React from 'react';
import {
    useHistory,
} from "react-router-dom";


const AdminNav = props => {
    const history = useHistory();
    const logout = () => {

        window.localStorage.clear()
        history.push('/');
    }
    return (
        <nav>
            <div>Logo</div>
            <div>Username</div>
            <button onClick={logout}>Logout</button>
        </nav>
    );
}


export default AdminNav