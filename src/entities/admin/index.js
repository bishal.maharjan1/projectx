import React from "react";
import AdminNav from "./containers/AdminNav";
import './index.scss';
import AdminSidebar from "./containers/AdminSidebar";

import {gql, useMutation} from '@apollo/client';

const GET_USER = gql`
    query MyQuery {
          type {
                    id
                    value
                }
            }`;

const Admin = props => {
    const [addUser, {data}] = useMutation(GET_USER);

    return (
        <main>
            <AdminNav/>
            <section>
                <AdminSidebar/>
                <div className="content">
                    <div className="form-group">
                        <label htmlFor="">random label</label>
                        <div className="">
                            <input type="text"/>
                        </div>
                        <span className="error-text"></span>
                    </div>
                </div>
            </section>
        </main>
    )
}

export default Admin;