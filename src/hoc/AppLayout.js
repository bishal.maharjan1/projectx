import React from 'react';
import {Switch, Route} from "react-router";
import routes from '../routes/';

const AppLayout = props => {
    return (
        <Switch>
            {routes.map((val, key) => (
                <Route component={val.component} path={val.path} exact={val.exact}/>
            ))}
        </Switch>
    )
}

export default AppLayout