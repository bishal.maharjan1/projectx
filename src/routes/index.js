import Auth from "../entities/auth";
import Admin from "../entities/admin";

export default [
    {path: '/', component: Auth, exact: true},
    {path: '/home', component: Admin, exact: true},
];