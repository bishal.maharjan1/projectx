import React from 'react';
import './App.scss';
import AppLayout from "./hoc/AppLayout";
import {ApolloClient, ApolloProvider, InMemoryCache} from '@apollo/client';

const client = new ApolloClient({
  uri: 'https://retrospective1213.herokuapp.com/v1/graphql',
  cache: new InMemoryCache()
});

function App() {
  return (
      <ApolloProvider client={client}>
         <AppLayout/>
      </ApolloProvider>
  );
}

export default App;
